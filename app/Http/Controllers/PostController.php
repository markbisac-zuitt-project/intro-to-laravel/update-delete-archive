<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use Auth;

class PostController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except(['index', 'show']);
    }
    
    // Create a new post
    // Endpoint: GET /posts/create
    public function create()
    {
        return view('posts.create');
    }

    // Endpoint: POST /posts
    public function store(Request $req)
    {
        // Create a new post object
        $new_post = new Post([
            'title' => $req->input('title'),
            'content' => $req->input('content'),
            'user_id' => Auth::user()->id
        ]);

        // Save it to the database
        $new_post->save();

        // Redirect the user somewhere
        return redirect('/posts/create');
    }

    // Endpoint: GET /posts
    public function index()
    {
        $posts_list = Post::all();
        return view('posts.index')->with('posts', $posts_list);
    }

    // Endpoint: GET /posts/{post_id}
    public function show($post_id)
    {
        // Retrieve a specific post
        $post = Post::find($post_id);
        return view('posts.show')->with('post', $post);
    }

    // Endpoint: GET /posts/my-posts
    public function myPosts()
    {
        $my_posts = Auth::user()->posts;
        return view('posts.index')->with('posts', $my_posts);
    }

    public function myArchive()
    {
        $post = Auth::user()->posts;
        $post->is_active = true;
        return view('posts.archives')->with('posts', $post);
    }

    // Endpoint: GET /posts/{post_id}/edit
    public function edit($post_id)
    {
        // Find the post to be updated
        $existing_post = Post::find($post_id);
        // Redirect the user to page where the post will be updated
        return view('posts.edit')->with('post', $existing_post);
    }

    // Endpoint: PUT /posts/{post_id}
    public function update($post_id, Request $req)
    {
        // Find an existing post to be updated
        $existing_post = Post::find($post_id);
        // Set the new values of an existing post
        $existing_post->title = $req->input('title');
        $existing_post->content = $req->input('content');
        $existing_post->save();
        // Redirect the user to the page of individual post
        return redirect("/posts/$post_id");
    }

    // Endpoint: DELETE /posts/{post_id}
    public function destroy($post_id)
    {
        // Find the existing post to be deleted
        $existing_post = Post::find($post_id);
        // Delete the post
        $existing_post->delete();
        // Redirect the user somewhere
        return redirect('/posts');
    }

    public function archive($post_id)
    {
        $existing_post = Post::find($post_id);

        $existing_post->is_active = false;

        $existing_post->save();

        return redirect("posts/$post_id/");
    }

  
}