@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <h1>{{ $post->title }}</h1>
        </div>
        <div class="col-md-8">
            <div class="row">
              <div class="card mb-3" style="width:100%;">
                <div class="card-body">
                  <h3 class="card-title">
                    <a href="#"><small>Posted by : {{$post->user->name}} </small></a>
                  </h3>
                  <p class="card-text">{{ $post->content }}</p>
                  <a href="/posts#{{$post->id}}">Click to back</a>
                </div>
              </div>
            </div>
        </div>
    </div>
</div>
@endsection